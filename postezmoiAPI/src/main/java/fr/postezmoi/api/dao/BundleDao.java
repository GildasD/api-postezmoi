package fr.postezmoi.api.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.postezmoi.api.model.Bundle;

@Repository
public interface BundleDao extends JpaRepository<Bundle, Integer> {

	public List<Bundle> findByOwnerId(Integer ownerId);
	
}