package fr.postezmoi.api.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.postezmoi.api.model.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

}
