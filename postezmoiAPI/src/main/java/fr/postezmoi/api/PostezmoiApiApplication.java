package fr.postezmoi.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostezmoiApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostezmoiApiApplication.class, args);
	}

}
