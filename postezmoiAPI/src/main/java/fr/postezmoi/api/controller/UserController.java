package fr.postezmoi.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.postezmoi.api.dao.BundleDao;
import fr.postezmoi.api.dao.UserDao;
import fr.postezmoi.api.model.Bundle;
import fr.postezmoi.api.model.User;

@RestController
public class UserController {

	@Autowired
	private UserDao userDao;

	@Autowired
	private BundleDao bundleDao;
	
	@PostMapping(value="/users", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public User createUser(User user) {
		return userDao.save(user);
	}
	
	@GetMapping(value="/users")
	public List<User> getUsers() {
		return userDao.findAll();
	}
	
	@GetMapping(value="/users/{id}")
	public User getUser(@PathVariable(value="id") Integer id) {
		return userDao.getOne(id);
	}
	
	@GetMapping(value="/users/{id}/bundles")
	public List<Bundle> getUserBundles(@PathVariable(value="id") Integer id) {
		final User user = userDao.getOne(id);
		return bundleDao.findByOwnerId(user.getId());
	}
	
	@GetMapping(value="/users/{userId}/bundles/{bundleId}")
	public Bundle getUserBundle(
			@PathVariable(value="userId") Integer userId,
			@PathVariable(value="bundleId") Integer bundleId
		) {
		return bundleDao.getOne(bundleId);
	}
	
}
